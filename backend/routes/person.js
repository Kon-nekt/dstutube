const { Router } = require("express");
const personController = require("../controllers/personController");
const authentiticate = require("../middlewares/adminAuth");

const router = Router();

router.get("/:id", async (req, res) => {
	personController.getPerson(req, res);
});

router.get("/", async (req, res) => {
	personController.getAllPersons(req, res);
});

router.get("/films/:id", async (req, res) => {
	personController.getPersonFilms(req, res);
});

router.post("/", async (req, res) => {
	personController.createPerson(req, res);
});

module.exports = router;
