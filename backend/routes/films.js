const { Router } = require("express");
const filmController = require("../controllers/filmController");
const authentiticate = require("../middlewares/adminAuth");

const router = Router();

router.get("/:id", async (req, res) => {
	filmController.getOnefilm(req, res);
});

router.get("/", async (req, res) => {
	filmController.getAllfilms(req, res);
});

router.get("/genre/:genre", async (req, res) => {
	filmController.getGenre(req, res);
});

router.post("/", async (req, res) => {
	filmController.createFilm(req, res);
});

router.delete("/:id", async (req, res) => {
	filmController.deleteFilm(req, res);
});

module.exports = router;
