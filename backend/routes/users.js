const { Router } = require("express");
const userController = require("../controllers/userController");
const authentiticate = require("../middlewares/authentiticateJWT");

const router = Router();

router.post("/watched/:id", authentiticate.authUser, async (req, res) => {
	userController.markWatched(req, res);
});

router.post("/like/:id", authentiticate.authUser, async (req, res) => {
	userController.likeFilm(req, res);
});

router.get("/", authentiticate.authUser, async (req, res) => {
	userController.getUser(req, res);
});

router.get("/watched", authentiticate.authUser, async (req, res) => {
	userController.getWatched(req, res);
});

module.exports = router;
