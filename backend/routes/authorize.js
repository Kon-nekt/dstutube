const { Router } = require('express');
const UserController = require('../controllers/userController');

const router = Router();

router.post('/login', async (req, res) => {
	UserController.login(req, res);
});

router.post('/register', async(req, res) => {
	UserController.register(req, res);
});

router.post('/refresh', async(req, res) => {
	UserController.refresh(req, res);
});

module.exports = router;
