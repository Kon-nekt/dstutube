const { Router } = require("express");
const compilationController = require("../controllers/compilationController");
const authentiticate = require("../middlewares/authentiticateJWT");

const router = Router();

router.get("/:id", async (req, res) => {
	compilationController.getCompilation(req, res);
});

router.get("/", async (req, res) => {
	compilationController.getAllCompilations(req, res);
});

router.get("/user/:id", async (req, res) => {
	compilationController.getUserCompilations(req, res);
});

router.post("/", authentiticate.authUser, async (req, res) => {
	compilationController.createCompilation(req, res);
});

module.exports = router;
