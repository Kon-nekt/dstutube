const filmModel = require("../models/film");
const compilationModel = require("../models/compilation");
const userModel = require("../models/user");

module.exports = {
	async getCompilation(req, res) {
		try {
			const compilation = await compilationModel
				.findOne({ uuid: req.params.id })
				.exec();
			return res.json({ payback: compilation });
		} catch (error) {
			console.log("Ошибка при получении подборки");
			return res.json({ error: "Ошибка при получении подборки" });
		}
	},

	async createCompilation(req, res) {
		try {
			const compilation = new compilationModel();
			compilation.owner = req.user.id;
			compilation.title = req.body.title;
			compilation.films = req.body.films;
			compilation.save();
			res.status(201);
			res.json(compilation);
		} catch (error) {
			console.log(error);
			return res.json({ error: "Ошибка" });
		}
	},

	async updateCompilation(req, res) {
		try {
			const compilation = await compilationModel.findOneAndUpdate(
				{ uuid: req.params.id },
				req.body
			);
			compilation.save();
			res.status(201);
			res.json(compilation);
		} catch (error) {
			console.log(error);
			return res.json({ error: "Ошибка" });
		}
	},

	async getAllCompilations(req, res) {
		try {
			const compilations = await compilationModel.find({});
			return res.json({ payback: compilations });
		} catch (error) {
			console.log("Ошибка при получении подборок");
			return res.json({ error: "Ошибка при получении подборок" });
		}
	},

	async getUserCompilations(req, res) {
		try {
			const user = await compilationModel
				.find({ owner: req.params.id })
				.exec();
			return res.json({ payback: user });
		} catch (error) {
			console.log("Ошибка при получении списка");
			return res.json({ error: "Ошибка при получении списка" });
		}
	},
};
