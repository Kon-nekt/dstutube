const filmModel = require("../models/film");

module.exports = {
	async getOnefilm(req, res) {
		try {
			const film = await filmModel.find({ uuid: req.params.id }).exec();
			return res.json({ payback: film });
		} catch (error) {
			console.log("Ошибка при получении фильма");
			return res.json({ error: "Ошибка при получении фильма" });
		}
	},

	async getAllfilms(req, res) {
		try {
			const films = await filmModel.find({});
			return res.json({ payback: films });
		} catch (error) {
			console.log("Ошибка при получении мероприятия");
			return res.json({ error: "Ошибка при получении фильмов" });
		}
	},

	async getGenre(req, res) {
		try {
			const genre = req.params.genre;
			const films = await filmModel.find({ genre: genre }).exec();
			return res.json({ payback: films });
		} catch (error) {
			console.log("Ошибка при получении мероприятий");
			return res.json({ error: "Ошибка при получении фильмов" });
		}
	},

	//TODO: move to admin(?)
	async createFilm(req, res) {
		try {
			const film = await filmModel.create(req.body);
			res.status(201);
			res.send();
		} catch (error) {
			console.log(error);
			return res.json({ error: "Ошибка при создании фильма" });
		}
	},

	async updateFilm(req, res) {
		try {
			const film = await filmModel.findOneAndUpdate(
				{ uuid: req.params.id },
				req.body
			);
			res.status(201);
			res.json(film);
		} catch (error) {
			console.log(error);
			return res.json({ error: "Ошибка при создании фильма" });
		}
	},

	async deleteFilm(req, res) {
		try {
			const film = await filmModel.findOneAndDelete({
				uuid: req.params.id,
			});
			res.status(200);
			res.send();
		} catch (error) {
			console.log(error);
			return res.json({ error: "Ошибка при создании фильма" });
		}
	},
};
