const personModel = require("../models/person");
const filmModel = require("../models/film");

module.exports = {
	async getPerson(req, res) {
		try {
			const person = await personModel
				.find({ uuid: req.params.id })
				.exec();
			return res.json({ payback: person });
		} catch (error) {
			console.log("Ошибка при получении");
			return res.json({ error: "Ошибка при получении" });
		}
	},

	async createPerson(req, res) {
		try {
			const person = await personModel.create(req.body);
			res.status(201);
			res.json(person);
		} catch (error) {
			console.log(error);
			return res.json({ error: "Ошибка" });
		}
	},

	async deletePerson(req, res) {
		try {
			await personModel.findOneAndDelete({ uuid: req.params.id });
			res.status(200);
			res.send();
		} catch (error) {
			console.log(error);
			return res.json({ error: "Ошибка" });
		}
	},

	async updatePerson(req, res) {
		try {
			const person = await personModel.findOneAndUpdate(
				{ uuid: req.params.id },
				req.body
			);
			res.status(200);
			res.json(person);
		} catch (error) {
			console.log(error);
			return res.json({ error: "Ошибка" });
		}
	},

	async getAllPersons(req, res) {
		try {
			const persons = await personModel.find({});
			return res.json({ payback: persons });
		} catch (error) {
			console.log("Ошибка при получении списка");
			return res.json({ error: "Ошибка при получении списка" });
		}
	},

	async getPersonFilms(req, res) {
		try {
			const films = await filmModel
				.find()
				.or([{ actor: req.params.id }, { director: req.params.id }]);
			return res.json({ payback: films });
		} catch (error) {
			console.log("Ошибка при получении списка");
			return res.json({ error: "Ошибка при получении списка" });
		}
	},
};
