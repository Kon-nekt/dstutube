const jwt = require("jsonwebtoken");
const redis = require("redis");
const { promisify } = require("util");
const crypto = require("crypto");
const UserModel = require("../models/user");
const filmModel = require("../models/film");

const client = redis.createClient("6379", "dstutube_redis_1");
const getAsync = promisify(client.get).bind(client);

module.exports = {
	async login(req, res) {
		try {
			if (Object.keys(req.body).length) {
				const user = await UserModel.findUser(req.body);
				if (user) {
					const accessToken = jwt.sign(
						{ id: user.uuid, role: user.role },
						process.env.ACCESS_SECRET,
						{ expiresIn: "1y" }
					);
					const refreshToken = jwt.sign(
						{ id: user.uuid, role: user.role },
						process.env.REFRESH_SECRET,
						{ expiresIn: "1y" }
					);
					client.set(
						crypto
							.createHash("sha256")
							.update(req.header("User-Agent"))
							.digest("hex"),
						refreshToken,
						redis.print
					);
					return res.json({ accessToken, refreshToken });
				}
			}
			return res.json({ error: "Неверная почта или пароль" });
		} catch (error) {
			logger.error("Ошибка входа", error);
			return res.json({ error: "Ошибка входа" });
		}
	},

	async refresh(req, res) {
		try {
			const { token } = await req.body;
			if (!token) {
				return res.sendStatus(401);
			}
			const refreshToken = await getAsync(
				crypto
					.createHash("sha256")
					.update(req.header("User-Agent"))
					.digest("hex")
			);
			if (!refreshToken) {
				return res.sendStatus(403);
			}
			if (token === refreshToken) {
				jwt.verify(token, process.env.REFRESH_SECRET, (err, user) => {
					if (err) {
						return res.sendStatus(403);
					}
					const accessToken = jwt.sign(
						{ username: user.email, role: user.role },
						process.env.ACCESS_SECRET,
						{ expiresIn: "2h" }
					);
					return res.json({ accessToken });
				});
			}
			return res.sendStatus(403);
		} catch (error) {
			logger.error("Ошибка входа", error);
			return res.json({ error: "Ошибка входа" });
		}
	},

	async register(req, res) {
		const check = await UserModel.findOne({ email: req.body.email }).exec();

		if (!check) {
			const user = new UserModel();
			user.email = req.body.email;
			user.password = req.body.password;
			await user.save();
			return this.login(req, res);
		}

		return res.json({
			error: "Пользователь с такой почтой уже существует",
		});
	},

	async markWatched(req, res) {
		try {
			const user = await UserModel.findOne({ uuid: req.user.id });
			user.watched.push(req.params.id);
			user.save();
			res.status(201);
			res.send();
		} catch (error) {
			console.log(error);
			return res.json({ error: "Ошибка" });
		}
	},

	async likeFilm(req, res) {
		try {
			const user = await UserModel.findOne({ uuid: req.user.id });
			user.liked.push(req.params.id);
			user.save();
			res.status(201);
			res.send();
		} catch (error) {
			console.log(error);
			return res.json({ error: "Ошибка" });
		}
	},

	async getUser(req, res) {
		try {
			const user = await UserModel.findOne({ uuid: req.user.id });
			return res.json(user);
		} catch (error) {
			console.log(error);
			return res.json({ error: "Ошибка" });
		}
	},

	async getWatched(req, res) {
		try {
			const user = await UserModel.findOne({ uuid: req.user.id });
			const films = await filmModel.find().or(user.watched);
			return res.json(films);
		} catch (error) {
			console.log(error);
			return res.json({ error: "Ошибка" });
		}
	},
};
