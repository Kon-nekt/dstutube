const jwt = require("jsonwebtoken");

module.exports = {
	async authUser(req, res, next) {
		try {
			const authHeader = req.headers.authorization;

			if (authHeader) {
				const token = authHeader.replace("Bearer ", "");

				const user = await jwt.verify(token, process.env.ACCESS_SECRET);
				if (user.role === "admin") req.user = user;
				else return res.json({ error: "Вы не администратор!" });
				return next();
			}
			return res.sendStatus(401);
		} catch (error) {
			console.log(error);
			if (error instanceof jwt.JsonWebTokenError) {
				return res.sendStatus(403);
			}
			return res.json({ error: "Ошибка при аутентификации" });
		}
	},
};
