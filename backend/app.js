require("dotenv").config();
var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var mongoose = require("mongoose");
var cors = require("cors");

var authRouter = require("./routes/authorize");
var userRouter = require("./routes/users");
var filmRouter = require("./routes/films");
var compilationRouter = require("./routes/compilation");
var personRouter = require("./routes/person");

var app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use("/static", express.static("public"));

app.use(cors());
app.use("/auth", authRouter);
app.use("/user", userRouter);
app.use("/film", filmRouter);
app.use("/compilation", compilationRouter);
app.use("/person", personRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

mongoose.connect(
	`mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_ADDRESS}/${process.env.DB_NAME}`,
	{ useNewUrlParser: true, useUnifiedTopology: true }
);

const db = mongoose.connection;

db.on("open", () => console.log("connection open"));

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get("env") === "development" ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.json({
		message: err.message,
		error: err,
	});
});

module.exports = app;
