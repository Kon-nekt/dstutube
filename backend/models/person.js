const mongoose = require("mongoose");
const uuid = require("node-uuid");

const personSchema = new mongoose.Schema({
	uuid: {
		type: String,
		default: function genUUID() {
			return uuid.v1();
		},
	},
	fName: {
		type: String,
		required: true,
	},
	sName: {
		type: String,
		required: true,
	},
	films: [
		{
			uuid: {
				type: String,
				required: true,
			},
			position: {
				type: String,
				required: true,
			},
		},
	],
});

module.exports = mongoose.model("Person", personSchema);
