const mongoose = require("mongoose");
const crypto = require("crypto");
const uuid = require("node-uuid");

const UserSchema = new mongoose.Schema({
	uuid: {
		type: String,
		default: function genUUID() {
			return uuid.v1();
		},
	},
	email: {
		type: String,
		default: "",
	},
	password: {
		type: String,
		default: "",
	},
	dateOfRegistration: {
		type: Date,
		default: Date.now,
	},
	role: {
		type: String,
		default: "member",
	},
	myCompilations: {
		type: [String],
		default: [],
	},
	otherCompilations: {
		type: [String],
		default: [],
	},
	watched: {
		type: [String],
		default: [],
	},
	liked: {
		type: [String],
		default: [],
	},
});

UserSchema.pre("save", function (next) {
	const user = this;
	const hash = crypto
		.createHash("sha256")
		.update(user.password)
		.digest("hex");
	user.password = hash;
	next();
});

UserSchema.static("findUser", async (User) => {
	const hash = crypto
		.createHash("sha256")
		.update(User.password)
		.digest("hex");
	return mongoose
		.model("User")
		.findOne({ email: User.email, password: hash })
		.exec();
});

UserSchema.static("markWatched", async (Film) => {
	return mongoose
		.model("User")
		.findOne({ email: User.email, password: hash })
		.exec();
});

module.exports = mongoose.model("User", UserSchema);
