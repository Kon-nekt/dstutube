const personController = require("../models/person");
const mongoose = require("mongoose");
const uuid = require("node-uuid");

const filmSchema = new mongoose.Schema({
	uuid: {
		type: String,
		default: function genUUID() {
			return uuid.v1();
		},
	},
	title: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		required: true,
	},
	premiere: {
		type: Date,
		required: true,
	},
	rating: {
		type: Number,
		required: true,
	},
	country: {
		type: String,
		required: true,
	},
	duration: {
		type: Number,
		default: 0,
	},
	director: {
		type: [String],
		default: [],
	},
	genre: {
		type: [String],
		default: [],
	},
	actor: {
		type: [String],
		default: [],
	},
	cover: {
		type: String,
		required: true,
	},
	trailer: {
		type: String,
		required: true,
	},
	screenshots: {
		type: String,
		required: true,
	},
});

filmSchema.post("save", async function (next) {
	const film = this;
	for (const actor of film.actor) {
		const person = await personController.findOne({ uuid: actor });
		if (person) {
			person.films.push({ uuid: film.uuid, position: "Актёр" });
			person.save();
		}
	}
	for (const director of film.director) {
		const person = await personController.findOne({ uuid: director });
		if (person) {
			person.films.push({ uuid: film.uuid, position: "Режиссёр" });
			person.save();
		}
	}
});

module.exports = mongoose.model("Film", filmSchema);
