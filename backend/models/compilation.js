const mongoose = require("mongoose");
const uuid = require("node-uuid");

const CompilationSchema = new mongoose.Schema({
	uuid: {
		type: String,
		default: function genUUID() {
			return uuid.v1();
		},
	},
	title: {
		type: String,
		required: true,
	},
	owner: {
		type: String,
		required: true,
	},
	films: {
		type: [String],
		default: [],
	},
});

module.exports = mongoose.model("Compilation", CompilationSchema);

CompilationSchema.static("createCompilation", async (compilation) => {
	return mongoose
		.model("compilation")
		.create({ title: compilation.title, owner: compilation.owner })
		.exec();
});
