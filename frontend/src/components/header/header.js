import React, { useState } from 'react';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import './header.css';

import Search from '../search/search'

import lg from './img/logo.png'



export default function Header() {

    const [header, setHeader] = useState(false)
    const [logo, setLogo] = useState(false)

    const changeBackground = () => {
        if (window.scrollY >= 150) {
            setHeader(true)
        } else {
            setHeader(false)
        }
    }

    const changeMargin = () => {
        if (window.scrollY >= 150) {
            setLogo(true)
        } else {
            setLogo(false)
        }
    }

    window.addEventListener('scroll', changeBackground)
    window.addEventListener('scroll', changeMargin)

    let headerli

    const output = () => {
        localStorage.setItem('login', 'null');
        window.location.reload();
    }

    if (localStorage.getItem('login') == 'user') {
        headerli = (
            <div className="login">
                <ul className="pen-main-header-list-item">
                    <li><a href="/"><span>Домой</span></a></li>
                    <li><a href="/genres"><span>Фильмы</span></a></li>
                    <li><a href="#"><span>Кабинет</span></a></li>
                    <li><a className="output" onClick={output}><span>Выход</span></a></li>
                </ul>
            </div>
        )
    } else {
        if (localStorage.getItem('login') == 'admin') {
            headerli = (
                <div className="login">
                    <ul className="pen-main-header-list-item">
                        <li><a href="/"><span>Домой</span></a></li>
                        <li><a href="/genres"><span>Фильмы</span></a></li>
                        <li><a href="/constructor"><span>Конструктор</span></a></li>
                        <li><a href="/destructor"><span>Удаление</span></a></li>
                        <li><a className="output" onClick={output}><span>Выход</span></a></li>
                    </ul>
                </div>
            )
        } else {
            headerli = (
                <div>
                    <ul className="pen-main-header-list-item">
                        <li><a href="/"><span>Домой</span></a></li>
                        <li><a href="/genres"><span>Фильмы</span></a></li>
                        <li><a href="/login"><span>Вход</span></a></li>
                        <li><a href="/registration"><span>Регистрация</span></a></li>
                    </ul>
                </div>
            )
        }
    }

    const buttomSearch = () => {
        console.log("dasdasdasdas")
        if (localStorage.getItem('login') == 'user'){
            console.log(localStorage.getItem('login'))
            window.location.assign('/player');
        } else {
            console.log(localStorage.getItem('login'))
            window.location.assign('/login');
        }
        
      }

    return (
        <header className={header ? 'header-active' : 'header'}>
            <div className={logo ? 'logo-active' : 'logo'}>
                <a href="/" className="pen-main-header-logo-link">
                    <img className="pen-main-header-logo-img" src={lg} />
                </a>
            </div>
            <div className="pen-main-header-search">
                <Search />
            </div>
            <div className="buttom-search">
                <Button aria-label="delete" onClick={buttomSearch}>
                    <SearchIcon/>
                </Button>
            </div>
            <div className="pen-main-header-list">
                {
                    headerli
                }
            </div>
        </header>
    )
}