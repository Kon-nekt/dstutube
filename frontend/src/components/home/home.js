import './home.css';

import Header from '../header';
import Carusel from '../carusel';
import SSlider from '../slider/slider';
import Films from '../films/films'
import Hover from '../hover/hover'

export default function Home() {
  let headerli

  if (localStorage.getItem('login') == 'user') {
    headerli = (
      <div>
        <Films/>
        <SSlider/>
        <Films/>
        <div className="film-bg-black">
          <Films/>
        </div>
        <Hover/>
      </div>
    )
  } else {
    if (localStorage.getItem('login') == 'admin') {
      headerli = (
        <div>
          <SSlider/>
        </div>
      )
    } else {
      headerli = null
    }
  }

  return (
    <div className="home-page">
      <Header/>
      <Carusel/>
      {
        headerli
      }
    </div>
  );
}