import React, { Component } from "react";
import Slider from "react-slick";

import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import './carusel.css';

import c1 from './img/c1.jpg';
import c2 from './img/c2.jpg';
import c3 from './img/c3.jpg';
import c4 from './img/c4.jpg';
import c5 from './img/c5.jpg';
import c6 from './img/c6.jpg';

const posters = [
  {
    name: 'c1',
    url: './img/c1.jpg'
  },
  {
    name: 'c2',
    url: './img/c2.jpg'
  },
  {
    name: 'c3',
    url: './img/c3.jpg'
  },
  {
    name: 'c4',
    url: './img/c4.jpg'
  },
  {
    name: 'c5',
    url: './img/c5.jpg'
  },
  {
    name: 'c6',
    url: './img/c6.jpg'
  },
]


export default function Carusel () {
    
  const settings = {
    className: "page-poster-carusel",
    dots: false,
    fade: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    arrows: false,
    slidesToScroll: 1,
    autoplaySpeed: 1
  };

  return (
    <div className="page-poster">
      <Slider {...settings}>
          <div className="page-poster-carusel-item">
            <a className="page-poster-carusel-item-link" href="#">
              <img className="page-poster-carusel-item-img"  src={c1} alt="caruselitem"/>
              <div className="item-img-stars"><h1>Артисты:</h1> Крис Хемсворт, Киллиан Мёрфи, Брендан Глисон</div>
              <div className="item-img-director"><h1>Режисcер:</h1> Рон Ховард</div>
              <div className="item-img-info">В 1819 году американский корабль «Эссекс» с командой из двух десятков человек на борту отправился из порта в штате Массачусетс на китобойный промысел. Осенью 1820-го охота была прервана атакой гигантского кашалота на судно, в результате чего морякам пришлось пересесть в шлюпки. В течение трех с лишним месяцев они боролись за выживание посреди океана…</div>
              <div className="item-img-name">В СЕРДЦЕ МОРЯ</div>
              </a>
          </div>
          <div className="page-poster-carusel-item">
            <a className="page-poster-carusel-item-link" href="#">
              <img className="page-poster-carusel-item-img"  src={c2} alt="caruselitem"/>
              <div className="item-img-stars"><h1>Артисты:</h1> Леонардо ДиКаприоб, Джозеф Гордон, Том Харди</div>
              <div className="item-img-director"><h1>Режисcер:</h1> Кристофер Нолан</div>
              <div className="item-img-info">Кобб – талантливый вор, лучший из лучших в опасном искусстве извлечения: он крадет ценные секреты из глубин подсознания во время сна, когда человеческий разум наиболее уязвим. Редкие способности Кобба сделали его ценным игроком в привычном к предательству мире промышленного шпионажа, но они же превратили его в извечного беглеца и лишили всего, что…</div>
              <div className="item-img-name">Начало</div>
            </a>
          </div>
          <div className="page-poster-carusel-item">
            <a className="page-poster-carusel-item-link" href="#">
            <img className="page-poster-carusel-item-img"  src={c3} alt="caruselitem"/>
              <div className="item-img-stars"><h1>Артисты:</h1> Леонардо ДиКаприоб, Джозеф Гордон, Том Харди</div>
              <div className="item-img-director"><h1>Режисcер:</h1> Кристофер Нолан</div>
              <div className="item-img-info">Кобб – талантливый вор, лучший из лучших в опасном искусстве извлечения: он крадет ценные секреты из глубин подсознания во время сна, когда человеческий разум наиболее уязвим. Редкие способности Кобба сделали его ценным игроком в привычном к предательству мире промышленного шпионажа, но они же превратили его в извечного беглеца и лишили всего, что…</div>
              <div className="item-img-name">Ла-ла ленд</div>
            </a>
          </div>
          <div className="page-poster-carusel-item">
            <a className="page-poster-carusel-item-link" href="#">
              <img className="page-poster-carusel-item-img"  src={c4} alt="caruselitem"/>
              <div className="item-img-stars"><h1>Артисты:</h1> Леонардо ДиКаприоб, Джозеф Гордон, Том Харди</div>
              <div className="item-img-director"><h1>Режисcер:</h1> Кристофер Нолан</div>
              <div className="item-img-info">Кобб – талантливый вор, лучший из лучших в опасном искусстве извлечения: он крадет ценные секреты из глубин подсознания во время сна, когда человеческий разум наиболее уязвим. Редкие способности Кобба сделали его ценным игроком в привычном к предательству мире промышленного шпионажа, но они же превратили его в извечного беглеца и лишили всего, что…</div>
              <div className="item-img-name">Авиатор</div>
            </a>
          </div>
          <div className="page-poster-carusel-item">
            <a className="page-poster-carusel-item-link" href="#">
              <img className="page-poster-carusel-item-img"  src={c5} alt="caruselitem"/>
              <div className="item-img-stars"><h1>Артисты:</h1> Леонардо ДиКаприоб, Джозеф Гордон, Том Харди</div>
              <div className="item-img-director"><h1>Режисcер:</h1> Кристофер Нолан</div>
              <div className="item-img-info">Кобб – талантливый вор, лучший из лучших в опасном искусстве извлечения: он крадет ценные секреты из глубин подсознания во время сна, когда человеческий разум наиболее уязвим. Редкие способности Кобба сделали его ценным игроком в привычном к предательству мире промышленного шпионажа, но они же превратили его в извечного беглеца и лишили всего, что…</div>
              <div className="item-img-name">Союзники</div>
            </a>
          </div>
          <div className="page-poster-carusel-item">
            <a className="page-poster-carusel-item-link" href="#">
              <img className="page-poster-carusel-item-img"  src={c6} alt="caruselitem"/>
              <div className="item-img-stars"><h1>Артисты:</h1> Леонардо ДиКаприоб, Джозеф Гордон, Том Харди</div>
              <div className="item-img-director"><h1>Режисcер:</h1> Кристофер Нолан</div>
              <div className="item-img-info">Кобб – талантливый вор, лучший из лучших в опасном искусстве извлечения: он крадет ценные секреты из глубин подсознания во время сна, когда человеческий разум наиболее уязвим. Редкие способности Кобба сделали его ценным игроком в привычном к предательству мире промышленного шпионажа, но они же превратили его в извечного беглеца и лишили всего, что…</div>
              <div className="item-img-name">Люди в чёрном 3</div>
            </a>
          </div>
      </Slider>
    </div>
  );
}