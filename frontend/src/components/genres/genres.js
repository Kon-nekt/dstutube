import React, { Component, useState, useEffect } from 'react';

import Header from "../header/header"
import Film from "../film/film"
import Hover from '../hover/hover'
import './genres.css';

export default function Genres (props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);

    useEffect(() => {
        fetch("http://5.63.153.160:8000/film/")
            .then(res => res.json())
            .then(
                result => {
                    setIsLoaded(true);
                    setItems(result.payback);
                },
            )
        }, [])
    

        if (error) {
            return <div>Ошибка: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Загрузка...</div>;
        } else {
            // console.log(items);
            // console.log(props.match.path); 
            const isItems = Array.isArray(items) && items?.length
            const filmsComponent = isItems ?  items.map((item, index) => 
                <Film key={index} film={item}/>
            ): null

            return (
                <div className="ganres-page-main">
                    <div className="bg-header">
                        <Header/>
                    </div>
                    <div className="Collection-ganres">
                        {
                            filmsComponent
                        }
                    </div>
                    <Hover/>
                </div>
            )
        }
}