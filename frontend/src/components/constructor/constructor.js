import React, { Component, useState, useEffect } from 'react';
import './constructor.css'
import Header from '../header'
import SelectorConstructor from '../selector'
import Picker from '../pickers'
import TextField from '../textfield'
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

let mTitle = null
let mRating = null
let mPremiere = null
let mCountry = null
let mGanres = null
let mAge = null

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

async function addFilm() {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
        "title": mTitle,
        "description": "Самый новый",
        "director": "Богатый парень",
        "premiere": "06.06.2006",
        "rating": 20,
        "genre": ["Блокбастер"],
        "country": "Россия",
        "duration": 96,
        "actor": ["8c725c00-9261-11eb-a21a-ed45c28fe253"],
        "cover": "https://image.tmdb.org/t/p/w1280/3d8b5SwdOGvlU2YHYfKrrDnFyFj.jpg",
        "trailer": "https://youtu.be/zw81ihoukKU",
        "screenshots": "https://i2.wp.com/itc.ua/wp-content/uploads/2019/08/Once_Upon_a_Time_in_Hollywood_i01.jpg?fit=770%2C546&quality=100&strip=all&ssl=1"
    });

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    fetch("http://5.63.153.160:8000/film/", requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
}

async function addaActer() {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
        "fName": "Станислав",
        "sName": "Иванов"
    });

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    fetch("http://5.63.153.160:8000/person/", requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
}


export default function Constructor() {
    const classes = useStyles();
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);

    const [rating, setRating] = useState('');
    const [premiere, setPremiere] = useState('');
    const [country, setCountry] = useState('');
    const [ganres, setGanres] = useState('');
    const [age, setAge] = useState('');
    const [title, setTitle] = useState('');

    const titleHandler = (e) => {
        setTitle(e.target.value)
        mTitle = e.target.value
    }

    const ratingHandler = (e) => {
        setRating(e.target.value)
        mRating = e.target.value
    }

    const premiereHandler = (e) => {
        setPremiere(e.target.value)
        mPremiere = e.target.value
    }

    const countryHandler = (e) => {
        setCountry(e.target.value)
        mCountry = e.target.value
    }

    const ganresHandler = (e) => {
        setGanres(e.target.value)
        mGanres = e.target.value
    }

    const ageHandler = (e) => {
        setAge(e.target.value)
        mAge = e.target.value
    }


    useEffect(() => {
        fetch("http://5.63.153.160:8000/person/")
            .then(res => res.json())
            .then(
                result => {
                    setIsLoaded(true);
                    setItems(result.payback);
                },
            )
    }, [])

    if (error) {
        return <div>Ошибка: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Загрузка...</div>;
    } else {

        let fullName = new Array();
        const isItems = Array.isArray(items) && items?.length
        const actorComponent = isItems ? items.map((item, index) => {
            let customObject = {
                title: item.fName + ' ' + item.sName
            }
            fullName.push(customObject)
        }
        ) : null

        const genres = [
            { title: 'Преключения' },
            { title: 'Драма' },
            { title: 'Боевик' },
            { title: 'Трилер' },
            { title: 'Семейное' },
            { title: 'Фантастика', },
            { title: 'Комедия' },
            { title: 'Криминальный' },
            { title: 'Хорор' }
        ]

        const labelGanres = "Жанры"
        const labelActor = "Актеры"

        let country
        return (
            <div className="constructor-main-page">
                <Header />
                <div className="Input-state">
                    <div id="constructor-box">
                        <div className="constructor-left">
                            <h1>Добавление фильма</h1>
                            <div className={classes.root}>
                                <Grid container spacing={3}>
                                    <Grid item xs={4}><TextField className={classes.paper} lab={"Имя"} value={age} onChange={e => ageHandler(e)}/></Grid>
                                    <Grid item xs={4}><TextField className={classes.paper} lab={"Фамилия"} value={country} onChange={e => countryHandler(e)} /></Grid>
                                    <Grid item xs={4}><Button onClick={addaActer} variant="contained" color="primary" size="large" >Добавить актера</Button></Grid>
                                    <Grid item xs={12}><SelectorConstructor className={classes.paper} options={fullName} label={"Актеры"} /></Grid>
                                    <Grid item xs={4}><TextField className={classes.paper} lab={"Возраст"} value={age} onChange={e => ageHandler(e)}/></Grid>
                                    <Grid item xs={4}><TextField className={classes.paper} lab={"Страна"} value={country} onChange={e => countryHandler(e)} /></Grid>
                                    <Grid item xs={4}><TextField className={classes.paper} lab={"Обложка"} /></Grid>
                                    <Grid item xs={12}><SelectorConstructor className={classes.paper} options={genres} label={"Жанры"} /></Grid>
                                    <Grid item xs={4}><TextField className={classes.paper} lab={"Описание"} /></Grid>
                                    <Grid item xs={4}><TextField className={classes.paper} lab={"Рейтинг"} value={rating} onChange={e => ratingHandler(e)}/></Grid>
                                    <Grid item xs={4}><TextField className={classes.paper} lab={"Продюсер"} /></Grid>
                                    <Grid item xs={4}><TextField className={classes.paper} lab={"Скриншот"} /></Grid>
                                    <Grid item xs={4}><TextField className={classes.paper} lab={"Название"} value={title} onChange={e => titleHandler(e)}/></Grid>
                                    <Grid item xs={4}><TextField className={classes.paper} lab={"Трейлер"} /></Grid>
                                    <Grid item xs={4} />
                                    <Grid item xs={4}><Picker className={classes.paper} /></Grid>
                                    <Grid item xs={4} />
                                    <Grid item xs={4} />
                                    <Grid item xs={0.25} />
                                    <Grid item xs={4}><Button onClick={addFilm} variant="contained" color="primary" size="large" >Добавить</Button></Grid>
                                    <Grid item xs={4} />
                                </Grid>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}