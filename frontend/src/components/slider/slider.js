import React, { Component } from "react";
import Slider from "react-slick";

import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import './slider.css';

import c1 from './img/c1.png'
import c2 from './img/c2.png'
import c3 from './img/c3.png'
import c4 from './img/c4.png'
import c5 from './img/c5.png'
import c6 from './img/c6.png'
import c7 from './img/c7.png'
import c8 from './img/c8.png'
import c9 from './img/c9.png'


export default function Sslider () {

    const settings = {
        className: "center",
        dots: false,
        slidesToShow: 9,
        slidesToScroll: 6,
        swipe: false,
        speed: 500,
        infinite: false,
        arrows: false
    };

    return (
        <div className="slider-menu-genres">
            <div className="slider-menu-genres-content">
                <div className="slider-menu-genres-title">
                    <h1>
                        Фильмы по жанрам
                    </h1>
                </div>
                <div className="slider-menu-genres-colection">
                    <Slider {...settings}>
                        <a className="slider-menu-genres-item-link" href="/genres/adventures">
                            <div className="slider-menu-genres-item">
                                <div className="slider-menu-genres-item-container">
                                    <img className="slider-menu-genres-item-img" src={c1} alt="caruselitem"/>
                                    <div className="slider-menu-genres-item-overlay">
                                        <div className="text">Приключения</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a className="slider-menu-genres-item-link" href="/genres/dramas">
                            <div className="slider-menu-genres-item">
                                <div className="slider-menu-genres-item-container">
                                    <img className="slider-menu-genres-item-img" src={c2} alt="caruselitem"/>
                                    <div className="slider-menu-genres-item-overlay">
                                        <div className="text">Драмы</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a className="slider-menu-genres-item-link" href="/genres/action">
                            <div className="slider-menu-genres-item">
                                <div className="slider-menu-genres-item-container">
                                    <img className="slider-menu-genres-item-img" src={c3} alt="caruselitem"/>
                                    <div className="slider-menu-genres-item-overlay">
                                        <div className="text">Боевики</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a className="slider-menu-genres-item-link" href="/genres/thrillers">
                            <div className="slider-menu-genres-item">
                                <div className="slider-menu-genres-item-container">
                                    <img className="slider-menu-genres-item-img" src={c4} alt="caruselitem"/>
                                    <div className="slider-menu-genres-item-overlay">
                                        <div className="text">Триллеры</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a className="slider-menu-genres-item-link" href="/genres/family">
                            <div className="slider-menu-genres-item">
                                <div className="slider-menu-genres-item-container">
                                    <img className="slider-menu-genres-item-img" src={c5} alt="caruselitem"/>
                                    <div className="slider-menu-genres-item-overlay">
                                        <div className="text">Семейные</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a className="slider-menu-genres-item-link" href="/genres/fantasy">
                            <div className="slider-menu-genres-item">
                                <div className="slider-menu-genres-item-container">
                                    <img className="slider-menu-genres-item-img" src={c6} alt="caruselitem"/>
                                    <div className="slider-menu-genres-item-overlay">
                                        <div className="text">Фэнтези</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a className="slider-menu-genres-item-link" href="/genres/comedy">
                            <div className="slider-menu-genres-item">
                                <div className="slider-menu-genres-item-container">
                                    <img className="slider-menu-genres-item-img" src={c7} alt="caruselitem"/>
                                    <div className="slider-menu-genres-item-overlay">
                                        <div className="text">Комедия</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a className="slider-menu-genres-item-link" href="/genres/criminal">
                            <div className="slider-menu-genres-item">
                                <div className="slider-menu-genres-item-container">
                                    <img className="slider-menu-genres-item-img" src={c8} alt="caruselitem"/>
                                    <div className="slider-menu-genres-item-overlay">
                                        <div className="text">Криминал</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a className="slider-menu-genres-item-link" href="/genres/horror">
                            <div className="slider-menu-genres-item">
                                <div className="slider-menu-genres-item-container">
                                    <img className="slider-menu-genres-item-img" src={c9} alt="caruselitem"/>
                                    <div className="slider-menu-genres-item-overlay">
                                        <div className="text">Ужасы</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </Slider>
                </div>
            </div>
        </div>
    );
}