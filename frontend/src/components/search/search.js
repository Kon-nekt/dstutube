import './search.css';
import React, { Component, useState, useEffect } from 'react';
import Chip from '@material-ui/core/Chip';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';


import sh from './search.png';

let top100Films = [];

const useStyles = makeStyles((theme) => ({
  root: {
    width: 500,
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

export default function Search() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    fetch("http://5.63.153.160:8000/film/")
      .then(res => res.json())
      .then(
        result => {
          setIsLoaded(true);
          setItems(result.payback);
        },
      )
  }, [])

  if (error) {
    return <div>Ошибка: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Загрузка...</div>;
  } else {
    console.log(items);
    top100Films = [];
    const isItems = Array.isArray(items) && items?.length
    const filmsComponent = isItems ? items.map((item, index) =>
      top100Films.splice(index, 0, { title: item.title })
    ) : null

    return (
      <div className={classes.root}>
        <Autocomplete
          id="size-small-standard"
          size="small"
          options={top100Films}
          getOptionLabel={(option) => option.title}
          defaultValue={top100Films[13]}
          renderInput={(params) => (
            <TextField {...params} variant="standard" label="Поиск" />
          )}
        />
        
      </div>
    );
  }
}