import React from "react"
import './film.css'

export default function Film(props) {

    const id = props.film.uuid
    const rating = props.film.rating
    const year = props.film.premiere.substring(0, 4)
    const country = props.film.country
    const ganre = props.film.genre.join(', ')
    const age = props.film.age
    const img = props.film.cover
    const title = props.film.title

    const player = () => {
        if ((localStorage.getItem('login') == 'user') && (localStorage.getItem('login') == 'user')) {
            window.location.assign('/player');
        } else {
            window.location.assign('/login');
        }
    }

    return(
        <div className="slider-menu-film-item">
            <a className="slider-menu-film-item-link" onClick={player}>
                <div className="slider-menu-film-item-container">
                    <img className="slider-menu-film-item-img" src = {img}/>
                    <div className="slider-menu-film-item-overlay">
                        <div className="slider-menu-film-item-overlay-info">
                            <div className="info-rating">{rating}</div>
                            <div className="info-year">{year}</div>
                            <div className="info-country">{country}</div>
                            <div className="info-genre">{ganre}</div>
                            <div className="info-time">{age}</div>
                        </div>
                    </div>
                </div>
            </a>
            <span className="slider-menu-film-item-namemovie">{title}</span>
        </div>
    )
}