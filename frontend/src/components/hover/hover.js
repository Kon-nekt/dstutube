import React, { Component } from "react";

import './hover.css';

export default function Hover () {

    return (
        <div className="hover">
            <div className="container">
			    <div className="footer-logo">
				    I am <span>Creator</span>
			    </div>
			<div className="footer-copy">~©~</div>
		</div>
        </div>
    );
}