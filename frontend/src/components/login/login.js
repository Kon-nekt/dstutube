import React, {useState} from 'react'
import './login.css';

export default function Login() {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const loginHandler = (e) => {
    setLogin(e.target.value)
  }

  const passwordHandler = (e) => {
    setPassword(e.target.value)
  }

  const loginSet = () => {
    if (login == 'admin') {
      localStorage.setItem('login', 'admin');
    } else {
      localStorage.setItem('login', 'user');
    }
    window.location.assign('/');
  }

  return (
    <div className="login-page-container">
        <div className="bg"></div>
        <div id="login-box">
            <div className="left">
                <h1>Вход</h1>
                <input type="text" name="username" placeholder="Имя пользователя" value={login} onChange={e => loginHandler(e)}/>
                <input type="password" name="password" placeholder="Пароль" value={password} onChange={e => passwordHandler(e)}/>
                <input onClick={loginSet} type="submit" name="signup_submit" value="Вход"/>
            </div>
        </div>
    </div>
  );
}