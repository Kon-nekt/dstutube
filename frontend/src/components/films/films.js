import React, { Component, useState, useEffect } from "react";
import Slider from "react-slick";

import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import './films.css';

import Film from "../film/film"

export default function Films () {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);

    const settings = {
        className: "center",
        dots: true,
        slidesToShow: 7,
        slidesToScroll: 6,
        swipe: false,
        speed: 500,
        infinite: false,
        arrows: false
    };

    useEffect(() => {
        fetch("http://5.63.153.160:8000/film/")
            .then(res => res.json())
            .then(
                result => {
                    setIsLoaded(true);
                    setItems(result.payback);
                },
            )
        }, [])

        if (error) {
            return <div>Ошибка: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Загрузка...</div>;
        } else {
            console.log(items); 
            const isItems = Array.isArray(items) && items?.length
            const filmsComponent = isItems ?  items.map((item, index) =>
                <Film key={index} film={item}/>
            ): null

            
            return (
            <div className="slider-menu-film">
                <div className="slider-menu-film-title">
                    <h1>
                        Фильмы для вас
                    </h1>
                </div>
                <div className="slider-menu-film-colection">
                    <Slider {...settings}>
                        {
                            filmsComponent
                        }
                    </Slider>
                </div>
            </div>
        );
    }
}