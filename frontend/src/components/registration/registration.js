import './registration.css';

export default function Rregistration() {
  return (
    <div className="registration-page-container">
        <div className="bg"></div>
        <div id="registration-box">
            <div className="left">
                <h1>Регистрация</h1>

                <input type="text" name="username" placeholder="Имя пользователя" />
                <input type="password" name="password" placeholder="Пароль" />
                <input type="password" name="password2" placeholder="Повторите пароль" />

                <input type="submit" name="signup_submit" value="Регистрация" />
            </div>
        </div>
    </div>
  );
}