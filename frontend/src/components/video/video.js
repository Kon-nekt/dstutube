import React from 'react'
import ReactPlayer from 'react-player'

// import video from "../video/video2.mp4"

import './video.css';

export default function VideoFilm () {

        const videoScr = "https://www.youtube.com/watch?v=zw81ihoukKU";
        const poster = "";

    return (
        <div>
            <ReactPlayer
                url = {videoScr}
                controls = {true}
                width = '1280px'
                height = '720px'
            />
        </div>
    );
}