import React from 'react';
import Chip from '@material-ui/core/Chip';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
    root: {
        width: 380,
        '& > * + *': {
            marginTop: theme.spacing(3),
        },
    },
}));

export default function SelConstructor(props) {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Autocomplete
                multiple
                id="tags-standard"
                margin="normal"
                options={props.options}
                size="small"
                getOptionLabel={(option) => option.title}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        label={props.label}
                    />
                )}
            />
        </div>
    );
}
