import './player.css';

import Video from "../video/"
import Header from "../header/"

export default function Player() {
  return (
    <div className="Page-Player">
      <Header/>
      <div className="player">
        <Video/>
      </div>
    </div>
  );
}