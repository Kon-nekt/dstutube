import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { width } from '@material-ui/system';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
    //   margin: theme.spacing(2),
      width: '170px'
    },
  },
}));

export default function BasicTextFields({lab}) {
  const classes = useStyles();

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <TextField 
        id="standard-basic" 
        size="small"
        rowsMax={4}
        label={lab} />
    </form>
  );
}