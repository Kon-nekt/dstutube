import './App.css';

import Registration from './components/registration'
import Home from './components/home'
import Login from './components/login'
import Genres from './components/genres'
import Player from './components/player'
import Constructor from './components/constructor'
import Destructor from './components/destructor'

import { BrowserRouter, Route } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Route exact path="/" component={Home}/>
        <Route path="/registration" component={Registration}/>
        <Route path="/login" component={Login}/>
        <Route path="/genres" component={Genres}/>
        <Route path="/destructor" component={Destructor}/>
        <Route path="/player" component={Player}/>
        <Route path="/constructor" component={Constructor}/>
      </div>
    </BrowserRouter>
  );
}

export default App;