FROM node:14.15.0-buster

# Set working directory

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    nano \
    unzip \
    git \
    curl \
    gnupg2 \
    libpq-dev \
    libpq5

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN groupadd -g 1001 www
RUN useradd -u 1001 -ms /bin/bash -g www www

# Copy existing application directory contents
COPY backend/ /var/www/backend

WORKDIR /var/www/backend
RUN npm i && npm i -g --force nodemon 
# # Copy existing application directory permissions
# COPY --chown=www:www . /var/www

# USER www
# Expose port 8000 and start node
EXPOSE 8000
WORKDIR /var/www/backend
CMD ["nodemon", "./bin/www"]