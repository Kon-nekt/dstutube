function docex {
	docker-compose -f docker-compose.yml run --rm -e COLUMNS=`tput cols` -e LINES=`tput lines` app ${@:-bash}
}

function dockerUp {
	docker-compose up -d
}

function dockerDown {
	docker-compose down
}

function reinstallModules {
	docex rm -rf node_modules/*
	docex npm i 
}

function dockerLogs {
	docker-compose logs -f --tail=100 $1 2>&1
}

function dockerRestart {
	docker-compose restart $1
}

case $1 in
	exec)
		docex ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} ${10} ${11};
		shift
	;;
	up)
		dockerUp;
		shift
	;;
	down)
		dockerDown;
		shift
	;;
	logs)
		dockerLogs ${2:-app};
		shift
	;;
	restart)
		dockerRestart ${2:-app};
		shift
	;;
	reinstall-modules)
		reinstallModules;
		shift
	;;
	*)
		echo -e "no action defined";
		exit 1;
	;;
esac
